//
//  vktestApp.swift
//  vktest
//
//  Created by Ilya Suprun on 14.07.2022.
//

import SwiftUI

@main
struct vktestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
